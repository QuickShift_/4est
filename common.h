#pragma once

#include <stdint.h>

#define ArrayCount(Array) sizeof((Array)) / sizeof(Array[0])

#define FRAME_TIME_MS 33
#define WINDOW_WIDTH 800
#define WINDOW_HEIGHT 800

typedef int8_t s8;
typedef int8_t s08;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;
typedef s32 b32;

typedef uint8_t u8;
typedef uint8_t u08;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float r32;
typedef double r64;
typedef float f32;
typedef double f64;

struct button_state
{
  b32 EndedDown;
  b32 Changed;
};

struct input
{
  s32 MouseWindowX;
  s32 MouseWindowY;
  s32 dMouseWindowX;
  s32 dMouseWindowY;

  s32 MouseX;
  s32 MouseY;
  s32 dMouseX;
  s32 dMouseY;
  f32 dt;

  f32 NormMouseX;
  f32 NormMouseY;
  f32 NormdMouseX;
  f32 NormdMouseY;

  s32 MouseWheel;
  s32 dMouseWheel;

  union
  {
    button_state Buttons[18];
    struct
    {
      button_state MouseLeft;
      button_state MouseRight;
      button_state MouseMiddle;

      button_state q;
      button_state s;
      button_state z;
      button_state x;
      button_state c;
      button_state v;
      button_state LeftAlt;
      button_state RightAlt;
      button_state LeftCtrl;
      button_state RightCtrl;
      button_state LeftShift;
      button_state RightShift;
      button_state Backspace;
      button_state Delete;
      button_state Escape;
    };
  };
};

namespace Platform
{
  float GetTimeInSeconds();
  void  SetHighDPIAwareness();
}
