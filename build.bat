@echo off

set IncludePaths= /I ..\ /I ..\include
set CommonCompilerFlags=-Od -MTd -nologo -fp:fast -fp:except- -Gm- -GR- -EHa- -Zo -Oi -WX -W4 -wd4201 -wd4100 -wd4189 -wd4505 -wd4127 -FC -Z7
set CommonCompilerFlags=-D_CRT_SECURE_NO_WARNINGS %CommonCompilerFlags%
set CommonLinkerFlags= -incremental:no -opt:ref ..\lib\SDL2main.lib ..\lib\SDL2.lib shcore.lib

IF NOT EXIST msvc_build mkdir msvc_build
pushd msvc_build

cl %CommonCompilerFlags% %IncludePaths% ..\*.cpp ..\win32\*.cpp /Fe: 4est /link %CommonLinkerFlags% /SUBSYSTEM:CONSOLE
set LastError=%ERRORLEVEL%
popd

IF NOT %LastError%==0 GOTO :end

msvc_build\4est.exe

:end
