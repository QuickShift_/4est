// NOTE(rytis): Decided to use a software renderer.
// NOTE(rytis): Will use SDL because:
// *Easier to port to other platforms.
// *Has got easy to use software renderer.
#include <SDL2/SDL.h>
#include <stdio.h>

#include "common.h"

static b32
Init(SDL_Window** Window)
{
  if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
  {
    printf("SDL ERROR: SDL_Init failed. %s\n", SDL_GetError());
    return false;
  }

  *Window = SDL_CreateWindow("4est", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);

  if(!Window)
  {
    printf("SDL ERROR: SDL_CreateWindow failed. %s\n", SDL_GetError());
    return false;
  }

  return true;
}

static b32
ProcessInput(input* OldInput, input* NewInput, SDL_Event* Event)
{
  *NewInput = *OldInput;
  NewInput->dMouseWindowX = 0;
  NewInput->dMouseWindowY = 0;
  while(SDL_PollEvent(Event))
  {
    switch(Event->type)
    {
      case SDL_QUIT:
      {
        return false;
      } break;
      case SDL_KEYDOWN:
      {
        if(Event->key.keysym.sym == SDLK_q)
        {
          NewInput->q.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_s)
        {
          NewInput->s.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_z)
        {
          NewInput->z.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_x)
        {
          NewInput->x.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_c)
        {
          NewInput->c.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_v)
        {
          NewInput->v.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_LALT)
        {
          NewInput->LeftAlt.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_RALT)
        {
          NewInput->RightAlt.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_LCTRL)
        {
          NewInput->LeftCtrl.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_RCTRL)
        {
          NewInput->RightCtrl.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_LSHIFT)
        {
          NewInput->LeftShift.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_RSHIFT)
        {
          NewInput->RightShift.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_BACKSPACE)
        {
          NewInput->Backspace.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_DELETE)
        {
          NewInput->Delete.EndedDown = true;
        }
        if(Event->key.keysym.sym == SDLK_ESCAPE)
        {
          NewInput->Escape.EndedDown = true;
          return false;
        }
      } break;
      case SDL_KEYUP:
      {
        if(Event->key.keysym.sym == SDLK_q)
        {
          NewInput->q.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_s)
        {
          NewInput->s.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_z)
        {
          NewInput->z.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_x)
        {
          NewInput->x.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_c)
        {
          NewInput->c.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_v)
        {
          NewInput->v.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_LALT)
        {
          NewInput->LeftAlt.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_RALT)
        {
          NewInput->RightAlt.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_LCTRL)
        {
          NewInput->LeftCtrl.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_RCTRL)
        {
          NewInput->RightCtrl.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_LSHIFT)
        {
          NewInput->LeftShift.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_RSHIFT)
        {
          NewInput->RightShift.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_BACKSPACE)
        {
          NewInput->Backspace.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_DELETE)
        {
          NewInput->Delete.EndedDown = false;
        }
        if(Event->key.keysym.sym == SDLK_ESCAPE)
        {
          NewInput->Escape.EndedDown = false;
          return false;
        }
      } break;
      case SDL_MOUSEBUTTONDOWN:
      {
        if(Event->button.button == SDL_BUTTON_LEFT)
        {
          NewInput->MouseLeft.EndedDown = true;
        }
        if(Event->button.button == SDL_BUTTON_RIGHT)
        {
          NewInput->MouseRight.EndedDown = true;
        }
        if(Event->button.button == SDL_BUTTON_MIDDLE)
        {
          NewInput->MouseMiddle.EndedDown = true;
        }
      } break;
      case SDL_MOUSEBUTTONUP:
      {
        if(Event->button.button == SDL_BUTTON_LEFT)
        {
          NewInput->MouseLeft.EndedDown = false;
        }
        if(Event->button.button == SDL_BUTTON_RIGHT)
        {
          NewInput->MouseRight.EndedDown = false;
        }
        if(Event->button.button == SDL_BUTTON_MIDDLE)
        {
          NewInput->MouseMiddle.EndedDown = false;
        }
      } break;
      case SDL_MOUSEWHEEL:
      {
        if(Event->wheel.direction == SDL_MOUSEWHEEL_NORMAL)
        {
          NewInput->MouseWheel -= Event->wheel.y;
        }
        else
        {
          NewInput->MouseWheel += Event->wheel.y;
        }
      } break;
      case SDL_MOUSEMOTION:
      {
        NewInput->MouseWindowX = Event->motion.x;
        NewInput->MouseWindowY = Event->motion.y;
        NewInput->dMouseWindowX += Event->motion.xrel;
        NewInput->dMouseWindowY += Event->motion.yrel;
      } break;
    }
  }

  for(u32 Index = 0; Index < sizeof(NewInput->Buttons) / sizeof(button_state); ++Index)
  {
    NewInput->Buttons[Index].Changed = OldInput->Buttons[Index].EndedDown == NewInput->Buttons[Index].EndedDown ? false : true;
  }

  NewInput->MouseX = NewInput->MouseWindowX;
  NewInput->MouseY = WINDOW_HEIGHT - NewInput->MouseWindowY;
  NewInput->dMouseX = NewInput->dMouseWindowX;
  NewInput->dMouseY = -NewInput->dMouseWindowY;

  NewInput->NormMouseX = (float)NewInput->MouseX / (float)(WINDOW_WIDTH);
  NewInput->NormMouseY = (float)NewInput->MouseY / (float)(WINDOW_HEIGHT);
  NewInput->NormdMouseX = (float)NewInput->dMouseX / (float)(WINDOW_WIDTH);
  NewInput->NormdMouseY = (float)NewInput->dMouseY / (float)(WINDOW_HEIGHT);

  NewInput->dMouseWheel = NewInput->MouseWheel - OldInput->MouseWheel;

  return true;
}

int
main(int ArgCount, char** ArgValues)
{
  Platform::SetHighDPIAwareness();

  SDL_Window* Window = nullptr;
  if(!Init(&Window))
  {
    return -1;
  }

  SDL_Event Event;

  input OldInput = {};
  input NewInput = {};

  ProcessInput(&OldInput, &NewInput, &Event);
  OldInput = NewInput;

  f32 LastFrameStart = Platform::GetTimeInSeconds();

  for(;;)
  {
    f32 CurrentFrameStart = Platform::GetTimeInSeconds();

    if(!ProcessInput(&OldInput, &NewInput, &Event))
    {
      break;
    }
    NewInput.dt = CurrentFrameStart - LastFrameStart;


    OldInput = NewInput;
    LastFrameStart = CurrentFrameStart;

    f32 CurrentTime = Platform::GetTimeInSeconds();
    s32 Delay = FRAME_TIME_MS - (s32)(1000 * (CurrentTime - CurrentFrameStart));
    if(Delay > 0)
    {
      SDL_Delay(Delay);
    }
    //printf("==============\nMouseX = %d\nMouseY = %d\nLeftClick = %s\ndt = %f\nDelay = %u\n", NewInput.MouseX, NewInput.MouseY, NewInput.MouseLeft.EndedDown ? "true" : "false", NewInput.dt, Delay);
  }

  SDL_DestroyWindow(Window);
  SDL_Quit();

  return 0;
}
